package main

import "fmt"

func main() {
	fmt.Println("Hello, playground")
	foo()

}

func foo() {
		for i := 0; i < 100; i++ {
		if i%2 == 0 {
			fmt.Println(i)
		}
	}

}

// go get -d github.com/GoesToEleven/go-programming

// go get github.com/GoesToEleven/go-programming
// go get github.com/GoesToEleven/GolangTraining
// go get github.com/GoesToEleven/golang-web-dev


